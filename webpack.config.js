const createExpoWebpackConfigAsync = require('@expo/webpack-config');

module.exports = async function(env, argv) {
  const config = await createExpoWebpackConfigAsync(
    {
      ...env,
      mode: env.mode || 'development', // Use the mode from the environment variables or default to 'development'
    },
    argv
  );

  // Customize the config as needed

  return config;
};
